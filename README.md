## DESCRIPTION

_r.stats.error_ computes and prints a series of indices used to compare two maps.

The calculated indices are:

* Mean error
* Mean absolute error
* Mean squared error
* Root mean square error
* Multiplicative bias

## AUTHOR

Laurent G. Courty, Instituto de Ingeniería UNAM, Mexico