#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
MODULE:    r.stats.error

AUTHOR(S): Laurent Courty

PURPOSE:    Calculate indices for evaluating forecasting models by
            comparing forecast and observed event.

COPYRIGHT: (C) 2016 by Laurent Courty

            This program is free software; you can redistribute it and/or
            modify it under the terms of the GNU General Public License
            as published by the Free Software Foundation; either version 2
            of the License, or (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU General Public License for more details.
"""

#%option G_OPT_R_INPUT
#% key: forecast
#% description: Forecast map
#% required: yes
#%end

#%option G_OPT_R_INPUT
#% key: observed
#% description: Observation map (reference)
#% required: yes
#%end

#%flag
#% key: g
#% description: Print results in shell style
#%end

#%flag
#% key: d
#% description: Calculate difference in degrees
#%end


from __future__ import division
import sys
import os
import numpy as np
try:
    import bottleneck as bn
except ImportError:
    bn = np
import grass.script as grass
from grass.pygrass import raster


class Stats(object):
    """Provide methods to calculate and print statistics"""
    def __init__(self, arr_f, arr_o, degree=False):
        self.arr_f = arr_f
        self.arr_o = arr_o
        self.is_degree = degree

    def calculate(self):
        """Calculate statistics"""
        if self.is_degree:
            abs_diff = np.fabs(self.arr_f - self.arr_o)
            diff = 180 - np.fabs(abs_diff - 180)
        else:
            diff = self.arr_f - self.arr_o
        self.me = bn.nanmean(diff)
        self.mse = bn.nanmean(diff ** 2)
        self.rmse = self.mse ** .5
        self.bias = bn.nanmean(self.arr_f) / bn.nanmean(self.arr_o)
        self.mae = bn.nanmean(np.fabs(diff))

    def print_results(self):
        """Print results in a human readable way"""
        print("Mean error: {:.3f}".format(self.me))
        print("Mean absolute error: {:.3f}".format(self.mae))
        print("Mean squared error: {:.3f}".format(self.mse))
        print("Root mean square error: {:.3f}".format(self.rmse))
        print("Multiplicative bias: {:.3f}".format(self.bias))

    def print_results_shell(self):
        """Print results in shell style"""
        print("me={}".format(self.me))
        print("mae={}".format(self.mae))
        print("mse={}".format(self.mse))
        print("rmse={}".format(self.rmse))
        print("bias={}".format(self.bias))


def read_raster_map(rast_name):
    """Read a GRASS raster and return a numpy array"""
    with raster.RasterRow(rast_name, mode='r') as rast:
        array = np.array(rast, dtype=np.float64)
    return array


def main():
    # Read raster maps as numpy arrays
    arr_forecast = read_raster_map(options['forecast'])
    arr_observed = read_raster_map(options['observed'])

    stats = Stats(arr_forecast, arr_observed, degree=flags['d'])
    stats.calculate()
    if flags['g']:
        stats.print_results_shell()
    else:
        stats.print_results()


if __name__ == "__main__":
    options, flags = grass.parser()
    sys.exit(main())
